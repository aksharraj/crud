package com.restapi.restapiproduct.controller;

import java.util.List;

import com.restapi.restapiproduct.Entity.Product;
import com.restapi.restapiproduct.service.ProductService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class productControllers {

    @Autowired
    public ProductService productService;

    @GetMapping("/product")
    public List<Product> getProducts() {
        return this.productService.getAllProducts();
    }

    @GetMapping("/product/{id}")
    public Product getProduct(@PathVariable("id") int id) {
        return productService.getProductById(id);
    }

    @PostMapping("/product")
    public void addProduct(@RequestBody Product pro) {
        this.productService.addProduct(pro);
    }

    @DeleteMapping("/product/{pid}")
    public void deleteProduct(@PathVariable("pid") int pid) {
        this.productService.deleteProduct(pid);
    }

    @PutMapping("/product/{id}")
    public void updateProduct(@RequestBody Product newpro, @PathVariable("id") int id) {
        this.productService.updateProduct(newpro, id);
    }
}