package com.restapi.restapiproduct.dao;

import com.restapi.restapiproduct.Entity.Product;

import org.springframework.data.repository.CrudRepository;

public interface ProductRepo extends CrudRepository<Product, Integer> {

    public Product findById(int id);

}
