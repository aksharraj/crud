package com.restapi.restapiproduct.service;

import java.util.List;

import com.restapi.restapiproduct.Entity.Product;
import com.restapi.restapiproduct.dao.ProductRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProductService {

    @Autowired
    public ProductRepo productrepo;

    /*
     * private static List<Product> pr = new ArrayList<>();
     * 
     * static { pr.add(new Product(1, "Product_A", 100)); pr.add(new Product(2,
     * "Product_B", 200)); pr.add(new Product(3, "Product_C", 300)); }
     */
    public List<Product> getAllProducts() {
        return (List<Product>) productrepo.findAll();
    }

    public Product getProductById(int id) {

        return productrepo.findById(id);

        /*
         * for (Product pp : pr) { if (pp.getId() == id) return pp; } return null;
         */

    }

    public Product addProduct(Product p) {

        Product pp = productrepo.save(p);
        return pp;

        /*
         * pr.add(p); return p;
         */
    }

    public void deleteProduct(int id) {

        productrepo.deleteById(id);

        /*
         * List<Product> ll = new ArrayList<>(); for (Product pp : pr) { if (pp.getId()
         * != id) ll.add(pp);
         * 
         * } pr = ll;
         */
    }

    public void updateProduct(Product newpro, int id) {

        productrepo.save(newpro);

        /*
         * List<Product> lll = new ArrayList<>(); for (Product pp : pr) { if (pp.getId()
         * == id) { pp.setName(newpro.getName()); pp.setPrice(newpro.getPrice()); }
         * lll.add(pp); } pr = lll;
         */
    }

}